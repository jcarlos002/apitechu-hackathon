package com.techu.backend.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Optional;

@Document(collection = "UUAA")
public class UuaaModel {

 //   @Id
    @NotNull
    private String id;
    private String uuaa;
    private String fecha;
    private UserModel users;


    public UuaaModel(){
    }

    public UuaaModel(String id, String fecha) {
        this.id = id;
        this.fecha = fecha;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getUuaa() {
        return uuaa;
    }

    public void setUuaa(String uuaa) {
        this.uuaa = uuaa;
    }
    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }


    public UserModel getUsers() {
        return users;
    }

    public void setUsers(UserModel users) {
        this.users = users;

    }
}
