package com.techu.backend.service;

import com.techu.backend.model.UserModel;
import com.techu.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.security.interfaces.DSAPublicKey;
import java.util.List;
import java.util.Optional;
import org.springframework.data.mongodb.core.MongoOperations;

@Service    // equivalente a @component para indicar la clase que da servicio
public class UserMongoService {

    @Autowired  //Indica que se cree al arrancar la aplicacion
    UserRepository userRepository;
    @Autowired
    MongoOperations mo;

    // READ
    public List<UserModel> findAll(){
        return  userRepository.findAll();
    }

    // CREATE
    public UserModel save(UserModel newProducto){
        return userRepository.save(newProducto);

    }
    // READ ID
    public Optional<UserModel> findById(String id){
        return userRepository.findById(id);
    }

    public UserModel myfindById(String id){

        return mo.findOne(new Query(Criteria.where("_id").is(id)), UserModel.class);
    }


    // DELETE
    public boolean delete(UserModel uuaaModel){
        try {
            userRepository.delete(uuaaModel);
            return true;
        } catch(Exception ex){
            return false;
        }
    }

}
