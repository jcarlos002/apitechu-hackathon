package com.techu.backend.service;

import com.techu.backend.model.UserModel;
import com.techu.backend.model.UuaaModel;
import com.techu.backend.service.UserMongoService;
import com.techu.backend.repository.ProductoRepository;
import com.techu.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service    // equivalente a @component para indicar la clase que da servicio servicio
public class UuaaMongoService {

    @Autowired  //Indica que se cree al arrancar la aplicacion
    ProductoRepository productoRepository;
    @Autowired
    UserRepository userRepository;

    private UserMongoService userService;


    // READ
    public List<UuaaModel> findAll(){
        return productoRepository.findAll();
    }

    // CREATE
    public UuaaModel save(UuaaModel newProducto){
 //       String newidusuario =  newProducto.getUsers().getId();
 //       UserModel newuser = userService.myfindById(newidusuario);
 //       newProducto.setUsers(newuser);
        return productoRepository.save(newProducto);
    }
    // READ ID
    public Optional<UuaaModel> findById(String id){
        return productoRepository.findById(id);
    }
    // DELETE
    public boolean delete(UuaaModel uuaaModel){
        try {
            productoRepository.delete(uuaaModel);
            return true;
        } catch(Exception ex){
            return false;
        }
    }

}
