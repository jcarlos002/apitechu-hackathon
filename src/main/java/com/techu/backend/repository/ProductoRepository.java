package com.techu.backend.repository;

import com.techu.backend.model.UuaaModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductoRepository extends MongoRepository<UuaaModel, String> {
}
