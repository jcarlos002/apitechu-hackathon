package com.techu.backend.controller;

import com.techu.backend.model.UuaaModel;
import com.techu.backend.model.UserModel;
import com.techu.backend.service.UuaaMongoService;
import com.techu.backend.service.UserMongoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
//@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("${url.base}")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class ProductoController {

    @Autowired
    private UserMongoService userService;
    @Autowired
    private UuaaMongoService uuaaService;
   // private UserMongoService userService;

    @GetMapping("")
    public String home()  {
        return "API REST Tech U! UUAA v1.0.0";
    }

    // GET todos los usuarios (collection)
    @GetMapping("/user")
    public List<UserModel> getUser() {
        return userService.findAll();
    }

    // GET todos las guardias (collection)
    @GetMapping("/uuaa")
    public List<UuaaModel> getProductos() {
        return uuaaService.findAll();
    }


    // POST para crear guardias
    @PostMapping("/uuaa")
    public UuaaModel postProducto(@RequestBody UuaaModel newProduct) {
        String newidusuario =  newProduct.getUsers().getId();
        UserModel newuser = userService.myfindById(newidusuario);
        newProduct.setUsers(newuser);
        uuaaService.save(newProduct);
        return newProduct;
    }
    // POST para crear usuarios
    @PostMapping("/user")
    public UserModel postUser(@RequestBody UserModel newUser) {

        userService.save(newUser);
        return newUser;
    }


    // GET a ua guardia por Id (instancia)
    @GetMapping("/uuaa/{id}")
    // Si en responseEntity se informa ResponseEntity<ProductoModel> solo se podrian devolver variables de ese tipo
    public Optional<UuaaModel> getProductoById(@PathVariable String id){
       return uuaaService.findById(id);
    }
    // PUT a una guardia (si existe lo reemplaza)
    @PutMapping("/uuaa")
    public void putProducto(@RequestBody UuaaModel productoToUpdate){
        uuaaService.save(productoToUpdate);
    }

    // DELETE para borrar un producto
    @DeleteMapping("/uuaa")
    public boolean  deleteProducto(@RequestBody UuaaModel productoToDelete) {
        return uuaaService.delete(productoToDelete);
    }

}
